/* PSR - Czech Technical University
 * Authors: Timur Uzakov, Danylo Begim
 */

/*! 
 *  \file      main.h       
 *  \brief     Motor position sending program.
 *  \details   This code receives motor position from encoder and sends it over UDP socket.
 *  \author    Timur Uzakov
 *  \author    Danylo Begim
 *  \version   1.0
 *  \date      2021-2022     
 *  \warning   Improper use can crash your application
 *  \copyright MIT License.
 */

#ifndef MAIN_H
#define MAIN_H
/*!
  \def PID_Controller_Priority
  Setting of the priority of controller task
*/
/*!
  \def UDP_Priority
  Setting of the priority of message sending task
*/
/*!
  \def WWW_priority
  Setting of the priority of web server task
*/

#define PID_Controller_Priority 110
#define UDP_Priority 105
#define WWW_priority 120

/*!
  \def  DELAY_BEETWEEN_UDP_MESSAGES
*/

#define DELAY_BEETWEEN_UDP_MESSAGES 100

#include <taskLib.h>
#include <stdio.h>
#include <kernelLib.h>
#include <semLib.h>
#include <intLib.h>
#include <iv.h>
#include <sysLib.h>
#include <inetLib.h>

#include <xlnx_zynq7k.h>

#include <stdint.h>
#include <stdbool.h>
#include <taskLib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <sockLib.h>
#include <string.h>
/*!
  \def REGISTER(base, offs)
  Sets the register address based on motor \a base and motor \a offs parameter
*/

#define REGISTER(base, offs) (*((volatile UINT32 *)((base) + (offs))))
/*!
  \def BIT(i) 
  Changes the \a i  th bit of a register
*/
#define BIT(i) ((1) << (i))

/*!
  \def MAX_BUF 
  Maximal buffer size
*/

#define MAX_BUF 1024
/*!
  \def CLOCK_RATE 
  Parameter for sysClkRateSet() function
*/

#define CLOCK_RATE 1000

/*!
  @param irq_count 
  Counter of messages from IRQ
*/
volatile unsigned irq_count;

/* MOTOR macros 		
 * 												*/

/*!
  \def MOTOR_PIN 
*/

#define MOTOR_PIN BIT(6)

/* See section FPGA registers for more information.						*/

/*!
  \def PMOD1_BASE
  Address of motor connection bus 1
*/
#define PMOD1_BASE 0x43c20000
/*!
  \def PMOD2_BASE
  Address of motor connection bus 2
*/
#define PMOD2_BASE 0x43c30000

/*!
  \def MOTOR_BASE
  Address of motor connection 
*/

#define MOTOR_BASE PMOD1_BASE

/* GPIO register definitions											*/
/* See Zynq-7000 Technical Reference Manual for more information		*/
/*     Section: 14.2.4 Interrupt Function (pg. 391, pg. 1348).			*/

/*!
 * \def MOTOR_IRQ_PIN
 *  Pin on GPIO selected for interrupt										
 *  Note: Each bit in a register refers to one pin. Setting some bit to `1`		
 *  also means which pin is selected.									
*/
#define MOTOR_IRQ_PIN BIT(2)
/*!
 * \def GPIO_DIRM
 * Setting a bit in DIRM to `1` makes the corresponding pin behave as an output,
 * for `0` as input.															
 * Note: So setting this to `MOTOR_IRQ_PIN` means, that this pin is an output	
 *       (which it is not so do not do it!).									
 *       This is similar with other GPIO/INT registers.										
*/ 
#define GPIO_DIRM         REGISTER(ZYNQ7K_GPIO_BASE, 0x00000284)

/*!
 * \def GPIO_INT_ENABLE
 * Writing 1 to a bit enables IRQ from the corresponding pin.	
*/ 
#define GPIO_INT_ENABLE   REGISTER(ZYNQ7K_GPIO_BASE, 0x00000290)
/*!
 * \def GPIO_INT_DISABLE
 * Writing 1 to a bit disables IRQ from the corresponding pin.					
*/
#define GPIO_INT_DISABLE  REGISTER(ZYNQ7K_GPIO_BASE, 0x00000294)
/*!
 * \def GPIO_INT_STATUS 
 * Bits read as `1` mean that the interrupt event has occurred on a corresponding pin.	
 * Writing `1` clears the bits, writing `0` leaves the bits intact.						
*/
#define GPIO_INT_STATUS   REGISTER(ZYNQ7K_GPIO_BASE, 0x00000298)
/*!
 * \def GPIO_INT_TYPE 
 * Setting TYPE to `0` makes interrupt level sensitive, `1` edge sensitive.			
*/
#define GPIO_INT_TYPE     REGISTER(ZYNQ7K_GPIO_BASE, 0x0000029c)
/*!
 * \def GPIO_INT_POLARITY 
 * Setting POLARITY to `0` makes interrupt active-low (falling edge),					
 *                     `1` active-high (raising edge).									
*/
#define GPIO_INT_POLARITY REGISTER(ZYNQ7K_GPIO_BASE, 0x000002a0)
/*!
 * \def GPIO_INT_ANY 
 * Setting ANY to `1` while TYPE is `1` makes interrupts act on both edges.
*/
#define GPIO_INT_ANY      REGISTER(ZYNQ7K_GPIO_BASE, 0x000002a4)

/*!
 * \def MOTOR_SR REGISTER 
 * FPGA register definition																
*/
#define MOTOR_SR REGISTER(MOTOR_BASE, 0x4)
/*!
 * \def MOTOR_SR_IRC_A_MON
 * FPGA register definition																
*/
#define MOTOR_SR_IRC_A_MON 8
/*!
 * \def MOTOR_SR_IRC_B_MON
 * FPGA register definition																
*/
#define MOTOR_SR_IRC_B_MON 9

/*!
 * \def INCREMENT
 * Encoder value increment, always equal to 1																
*/
#define INCREMENT 1

int PIDReqValue;
int IRQCounter;
float PID_OUT;
char * IP_Target;
char * IP_BOARD;


/*! \fn void sender(void)
  	\brief	Tracking of the position increment/decrement and sending to the socket
 	\retval void
*/
void sender(void);


/*! \fn void irc_isr(void)
 *  \brief This function will be called by Interrupt Service Routine
 *  This function reads boolean values from the encoder,
 *  and transfers the information to increase or decrease
 *  of the position of the motor.
 *  \retval void
 * 
*/

void irc_isr(void);

/*! \fn void irc_init(void)
 *  \brief This function initializes the IRC sensor 
 *  \retval void
 *  \param ip_addr IP address of board to connect to
 *  \param port UDP socket port
 */

void irc_init(void);

/*! \fn  void irc_cleanup(void)
 *  \brief This function disconnects IRC sensor
 *  \retval void
 * 
 */

void irc_cleanup(void);

/*! \fn void motor(char * ip_address, int port)
 * \brief This function is Entry Point for DKM, where receiving of signal is occured
 * \retval void
 * \param ip_address IP address of board to connect to
 * \param port UDP socket port
 */

void motor(char * ip_address, int port);


/*!
 * \fn void PID_Calc()
 * \brief This function is the PID controller that calculates all the PID_OUT value.
 * \retval void
 * 
 */

void PID_Calc();






#endif
