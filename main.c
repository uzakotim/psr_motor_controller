/* PSR - Czech Technical University
 * Authors: Timur Uzakov, Danylo Begim
 */



#include "main.h"

#define READER_DELAY 1000
#define MAX_BUF 1024


int current_values[2];
int past_values[2];
int position = 0;


/*variables for Ethernet connection establishment*/
int addrlen;
char buf[MAX_BUF];
int sockd;
struct sockaddr_in my_addr, srv_addr;
char message[MAX_BUF];


void irc_isr(void)
{		
		past_values[0] = current_values[0];
	    past_values[1] = current_values[1];
	    
		bool irc_a = (MOTOR_SR & BIT(MOTOR_SR_IRC_A_MON)) != 0;
        bool irc_b = (MOTOR_SR & BIT(MOTOR_SR_IRC_B_MON)) != 0;
        
        current_values[0] = irc_a;
        current_values[1] = irc_b;
        
        if(((current_values[0] == 0)&&(current_values[1] == 0))&&((past_values[0] == 0)&&(past_values[1] == 1)))
		{
        	position += INCREMENT;
		}
		if((current_values[0] == 1)&&(current_values[1] == 0)&&(past_values[0] == 0)&&(past_values[1] == 0))
		{
			position += INCREMENT;
		}
		if((current_values[0] == 1)&&(current_values[1] == 1)&&(past_values[0] == 1)&&(past_values[1] == 0))
		{
			position += INCREMENT;
		}
		if((current_values[0] == 0)&&(current_values[1] == 1)&&(past_values[0] == 1)&&(past_values[1] == 1)){
			position += INCREMENT;
		}
								   
		if((current_values[0] == 0)&&(current_values[1] == 0)&&(past_values[0] == 1)&&(past_values[1] == 0)){
			position -= INCREMENT;
		}
		if((current_values[0] == 0)&&(current_values[1] == 1)&&(past_values[0] == 0)&&(past_values[1] == 0)){
			position -= INCREMENT;
		}
		if((current_values[0] == 1)&&(current_values[1] == 1)&&(past_values[0] == 0)&&(past_values[1] == 1)){
			position -= INCREMENT;
		}
		if((current_values[0] == 1)&&(current_values[1] == 0)&&(past_values[0] == 1)&&(past_values[1] == 1)){
			position -= INCREMENT;
		}
		
        irq_count++;
        GPIO_INT_STATUS = MOTOR_IRQ_PIN; /* clear the interrupt */
}

void irc_init(void)
{
        GPIO_INT_STATUS = MOTOR_IRQ_PIN; /* reset status */
        GPIO_DIRM = 0x0;                 /* set as input */
        GPIO_INT_TYPE = MOTOR_IRQ_PIN;   /* interrupt on edge */
        GPIO_INT_POLARITY = 0x0;         /* falling edge */
        GPIO_INT_ANY = 0x0;              /* ignore rising edge */
        GPIO_INT_ENABLE = MOTOR_IRQ_PIN; /* enable interrupt on MOTOR_IRQ pin */

        intConnect(INUM_TO_IVEC(INT_LVL_GPIO), irc_isr, 0);
        intEnable(INT_LVL_GPIO);         /* enable all GPIO interrupts */
}

void irc_cleanup(void)
{
        GPIO_INT_DISABLE = MOTOR_IRQ_PIN;

        intDisable(INT_LVL_GPIO);
        intDisconnect(INUM_TO_IVEC(INT_LVL_GPIO), irc_isr, 0);
}

void sender(void)
{
         
	while(1) 
	{			
		sprintf(buf, "%d", position);
		sendto(sockd, buf, sizeof(position), 0,
						(struct sockaddr*)&srv_addr, sizeof(srv_addr));
		printf("%d\n",position);
		taskDelay(READER_DELAY); 
	}
}



void motor(char * ip_address, int port)
{
	
		printf("Sending motor started\n");
        TASK_ID st;
		
	 	sysClkRateSet(1000);
	    current_values[0] = 0;
	    current_values[1] = 0;
	    

		/* Create a UDP socket */
		sockd = socket(AF_INET, SOCK_DGRAM, 0);
		if (sockd == -1)
		{
			perror("Socket creation error");
			exit(1);
		}
		/* Configure client address */
		my_addr.sin_family = AF_INET;
		my_addr.sin_addr.s_addr = INADDR_ANY;
		my_addr.sin_port = 0;    
		bind(sockd, (struct sockaddr*)&my_addr, sizeof(my_addr));
		
		/* Set server address */
		srv_addr.sin_family = AF_INET;
		inet_aton(ip_address, &srv_addr.sin_addr);
		srv_addr.sin_port = htons(port);
		
		irc_init();
	    st = taskSpawn("tPoseSend", 100, 0, 4096, (FUNCPTR) sender, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
}
