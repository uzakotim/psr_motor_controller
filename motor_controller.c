/* PSR - Czech Technical University
 * Authors: Timur Uzakov, Danylo Begim
 */
/*! 
 *  \file      motor_controller.c     
 *  \brief     UDP listente, solving PID postion, accept WWW requests
 *  \details   This code recive postion by using UDP connection, After program adjust postion of the motor using PID controller to the requested position.Also this program recivec HTTP requests and generates the hTML page with graphs
 *  \author    Timur Uzakov
 *  \author    Danylo Begim
 *  \version   1.0
 *  \date      2021-2022     
 *  \warning   Improper use can crash your application
 *  \copyright MIT License.
 */


#include "motor_controller.h"
#include "main.h"

/*!
  \def PID_FREQ 
  Setting the number of tick for delaying PID controller
*/
#define PID_FREQ 10
//#define delay_t 3
/*!
  \var volatile int actual_pose
 	 Variagle whertwe the actual postion of motor is stored
*/
volatile int actual_pose    = 0;

int          current_values[2];
/*!
  \var volatile int desired_pose 
 	variable where desired position accepted from UDP connection is stored
*/
volatile int desired_pose   = 0;
/*!
  \var float        error 
 	 Used for PID calculation
*/
float        error          = 0;
/*!
  \var float  integral 
 	  Used for PID calculation
*/
float        integral       = 0;
/*!
  \var int  past_values[2];
 	 Used for PID calculation
*/
int          past_values[2];
/*!
  \var float error_prior = 0;
 	  Used for PID calculation
*/
float 		 error_prior = 0;
/*!
  \var float derivative= 0;
 	 Used for PID calculation
*/
float 		 derivative= 0;
/*!
  \var float Bias=0;
 	  Used for PID calculation
*/
float 		 Bias=0;
/*!
  \var float integral_prior=0;
 	  Used for PID calculation
*/
float 		 integral_prior=0;


//int pwm_previuos_previous=0;
//int pwm_previos=0;

/*PID parameters*/
/*!
  \var float K_p
 	  Proportial gain 
*/
float K_p         =3; //0.950
/*!
  \var float K_i  
 	  Integration gain
*/
float K_i         =1; //0.0005
/*!
  \var float K_d
 	  Derivative gain
*/
float K_d		  =0;
/*!
  \var int pwm_value
 	 Current value of PWM if more then abs(200) then  it will be maximum 200
*/
int pwm_value= 0;
int count=0;

/*Web Site parameters*/
/*!
  \var int DesPos_max, int DesPos_min, int ActPos_max, int ActPos_min
 	 Vriable for storing the max and min valeus of ActPos_data array and DesPos_data array
*/
int DesPos_max=0;
int DesPos_min=0;
int ActPos_max=0;
int ActPos_min=0;

void pose_getter(int port)
{
    	
    
}


void init_motor_r(void)
{
	PWM_ENABLE |= 1<<6;
	PWM_PERIOD |= PWM_PERIOD_VALUE;
}

int findMin(int arr[])
{
    int i;
 
    int min = arr[0];

    for (i = 0; i < MAX_DATA; i++)
    {
       
        if (arr[i] < min)
        {
            min = arr[i];
        }
    }
 return min;
}

int findMax(int arr[])
{
    int i;
 
    int max = arr[0];

    for (i = 0; i < MAX_DATA; i++)
    {
        if (arr[i] > max)
        {
            max = arr[i];
        }
    }
    return max;
}
void pwm_control(void)
{
    for(;;)
    {
    
        error = desired_pose - actual_pose;
        if(abs(error)==0)
        {
           integral=0;
           pwm_value=0;
           error=0;
           DUTY_PWM |= pwm_value;
        }
        integral+= error;
        
        if(integral > 200)
        {
        	integral=200;
        	
        }
        if(integral<-200)
        {
           integral=-200;
        }
        
        pwm_value=(int)(K_p*error + K_i*integral+K_d*(error - error_prior));
        
        if (pwm_value >PWM_MAX)
        { 
        	pwm_value = PWM_MAX;
        	            
        }
        		
        if(pwm_value<-PWM_MAX)
        {
        			
        	pwm_value = -PWM_MAX;
        }
        
        DUTY_PWM &= 0;
        	
        if (pwm_value>0)
        {
        	        
        	DUTY_PWM |= (UINT32)pwm_value;
        	DUTY_PWM |= 1<<30; /*0 1 for counter-clockwise*/
        	DUTY_PWM &= ~(1<<31); /*bit setter, value 1 is set to bit 31 */
        				
        }
        
        if (pwm_value<0)
        {
        	pwm_value=abs(pwm_value);
        	DUTY_PWM |=(UINT32)pwm_value;
        	DUTY_PWM &= ~(1<<30); /* 1 0 for clock-wise; */
        	DUTY_PWM |= 1<<31;
        }
        			  //printf("REGISTER: %d",DUTY_PWM);
        			  //printf("PWM: %d\n",pwm_value);
        			  //printf("ERROR: %f\n",error);
        			  //printf("INT: %f\n\n\n",integral);
        			  
    
		//pwm_previos=pwm_value;
		//pwm_previuos_previous=pwm_previos;
		error_prior = error;
        taskDelay(PID_FREQ);
	}
}

void rotateArr(int arr[], int n,int newval)
{
    int i=0;
    
    for (i = 0; i < n - 1; i++)
    {
    	 arr[i] = arr[i + 1];
    }
    arr[i] = newval;
}
 
void storeVal(int desired,int actual,int pwm)
{
    if(count<MAX_DATA-1)
    {
        DesPos_data[count] = desired;
        //printf("DesPos: %d",desired);
        ActPos_data[count] = actual;
        //printf("ActPos: %d",actual);
        PWM_data[count] = pwm;
        //printf("PWM_data: %d",pwm);
        count++;
        
    }else
    {
    	rotateArr(DesPos_data,MAX_DATA-1,desired);
    	rotateArr(ActPos_data,MAX_DATA-1,actual);
    	rotateArr(PWM_data,MAX_DATA-1,pwm);
    }
   
    
}


void genarateHTML(int fd)
{
	int i=0;
	int graphtime=0;
			FILE *f  = fdopen(fd, "w");
		 	fprintf(f,"HTTP/1.0 200 OK\r\n");
		 	fprintf(f,"Current time is %ld.\rn", time(NULL));
		 	fprintf(f,"Content-Type: text/html\r\n");
		 	fprintf(f,"Accept-Ranges: bytes\r\n Content-Length: 1329 \r\n\r\n");
		    fprintf(f,("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML//EN\">\n<html>\n<head>\n<title>Graphs from</title>\n</head>\n<body>\n"));
		    fprintf(f,"<body onload=\"setTimeout(function(){location.reload()}, 500);\">\n<script>document.write(Date.now());</script>\n");
		    fprintf(f,("<h1>PWM [/%/]</h1>\n<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"500\" height=\"300\">\n\
		    <g transform=\"translate(50,130) scale(1)\">\n\
		        <g style=\"stroke-width:2; stroke:black\">\n\
		            <path d=\"M 0 100 L 400 100 Z\"/>\n\
		            <path d=\"M 0 -100 L 0 100 Z\"/>\n\
		    </g>\n\
		        <g style=\"fill:none; stroke:#B0B0B0; stroke-width:1; stroke-dasharray:2 4;text-anchor:end; font-size:30\">\n\
		            <text style=\"fill:black; stroke:none\" x=\"-1\" y=\"120\">0</text>\n\
		            <text style=\"fill:black; stroke:none\" x=\"-1\" y=\"-100\">100</text>\n\
		        <g style=\"text-anchor:middle\">\n<text style=\"fill:black; stroke:none\" x=\"100\" y=\"20\"></text>\n\
		            <text style=\"fill:black; stroke:none\" x=\"200\" y=\"20\"></text>\n\
		            <text style=\"fill:black; stroke:none\" x=\"300\" y=\"20\"></text>\n\
		            <text style=\"fill:black; stroke:none\" x=\"400\" y=\"20\"></text>\n\
		    </g>\n\
		    </g>\n"));
		    fprintf(f,("<polyline points=\"" ));
		    /*PWM percent graph*/
		    for(i=0;i<MAX_DATA; i++)
		    {
		    	if(PWM_data[i]!=0)
		    	{
		    		 fprintf(f,"%d,%d     ",graphtime,-(PWM_data[i]/PWM_MAX)*200+100);
		    		 raphtime++;
		    	}
		    			    	
		       
		    }
		    graphtime=0;
		    fprintf(f,"\" style=\"stroke:red; stroke-width: 1; fill : none;\"/>\n</g>\n</svg>\n");
		    /*Actual pos graph*/
		    fprintf(f,"<h1>Position</h1>\n<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"500\" height=\"300\">\n<g transform=\"translate(50,130) scale(1)\">\n<g style=\"stroke-width:2; stroke:black\">\n\
		          <path d=\"M 0 0 L 800 0 Z\" />\n\
		          <path d=\"M 0 -100 L 0 100 Z\" />\n\
		        </g>\n\
		        <g style=\"fill:none; stroke:#B0B0B0; stroke-width:1; stroke-dasharray:2 4;text-anchor:end; font-size:30\">\n\
		          <text style=\"fill:black; stroke:none\" x=\"50\" y=\"120\">%d</text>\n\
		          <text style=\"fill:black; stroke:none\" x=\"1\" y=\"0\">0</text>\n\
		          <text style=\"fill:black; stroke:none\" x=\"50\" y=\"-100\">%d</text>\n\
		          <g style=\"text-anchor:middle\">\n\
		            <text style=\"fill:black; stroke:none\" x=\"100\" y=\"130\"></text>\n\
		            <text style=\"fill:black; stroke:none\" x=\"200\" y=\"130\"></text>\n\
		            <text style=\"fill:black; stroke:none\" x=\"300\" y=\"130\"></text>\n\
		            <text style=\"fill:black; stroke:none\" x=\"400\" y=\"130\"></text>\n\
		          </g>\n\
		        </g>\n",-ActPos_max,ActPos_max);
		    fprintf(f,("<polyline points=\""));
		    for(i=0;i<MAX_DATA; i++)
		  	{
		    	
		    	if(ActPos_data[i]!=0)
		    	{
		  		
		  			fprintf(f,"%d,%d     ",graphtime,(ActPos_data[i]*100)/ActPos_max);
		  			graphtime++;
		    	}
		  				    		
		  		
		  	}
		    graphtime=0;
		    fprintf(f,"\" style=\"stroke:red; stroke-width: 1; fill : none;\"/>\n</g>\n</svg>\n");
		    fprintf(f,"<h1>Requsted Position</h1>\n\
		    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"500\" height=\"310\">\n\
		      <g transform=\"translate(50,130) scale(1)\">\n\
		        <g style=\"stroke-width:2; stroke:black\">\n\
		          <path d=\"M 0 0 L 800 0 Z\" />\n\
		          <path d=\"M 0 -100 L 0 100 Z\" />\n\
		        </g>\n\
		        <g style=\"fill:none; stroke:#B0B0B0; stroke-width:1; stroke-dasharray:2 4;text-anchor:end; font-size:30\">\n\
		          <text style=\"fill:black; stroke:none\" x=\"50\" y=\"120\">%d</text>\n\
		          <text style=\"fill:black; stroke:none\" x=\"1\" y=\"0\">0</text>\n\
		          <text style=\"fill:black; stroke:none\" x=\"50\" y=\"-100\">%d</text>\n\
		          <g style=\"text-anchor:middle\">\n\
		            <text style=\"fill:black; stroke:none\" x=\"100\" y=\"130\"></text>\n\
		            <text style=\"fill:black; stroke:none\" x=\"200\" y=\"130\"></text>\n\
		            <text style=\"fill:black; stroke:none\" x=\"300\" y=\"130\"></text>\n\
		            <text style=\"fill:black; stroke:none\" x=\"400\" y=\"130\"></text>\n\
		          </g>\n\
		        </g>\n",-DesPos_max,DesPos_max);
		    fprintf(f,("<polyline points=\"" ));
		    for(i=0;i<MAX_DATA; i++)
		    {
		    	
		    	if(DesPos_data[i]!=0)
		    	{
		    		fprintf(f,"%d,%d     ",graphtime,(DesPos_data[i]*100)/DesPos_max);
		    		graphtime++;
		    	}
		    
				    		
				  
		    }
		    graphtime=0;
		    fprintf(f,"\" style=\"stroke:red; stroke-width: 1; fill : none;\"/>\n</g>\n</svg>\n");
		    fprintf(f,"</g>\n</svg>\n</body>\n</html>\n");
		    fclose(f);
		    printf("Sended");

		    
}
void www_rutine(int argc, char *argv[])
{
	int     s;
    int     newFd;
    struct  sockaddr_in serverAddr;
    struct  sockaddr_in clientAddr;
    int     sockAddrSize;

    sockAddrSize = sizeof(struct sockaddr_in);
	memset(&serverAddr,0,sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(SERVER_PORT);
    serverAddr.sin_addr.s_addr = INADDR_ANY;
	int i=0;
	
	for(i=0;i<MAX_DATA;i++)
	{
		PWM_data[MAX_DATA]=0;
 		ActPos_data[MAX_DATA]=0;
 		DesPos_data[MAX_DATA]=0;
 		Time_data[MAX_DATA]=i;
	}


    printf("WWW_R starting\n");
    s=socket(AF_INET, SOCK_STREAM, 0);



    if (s<0)
    {

        printf("Error: www: socket(%d)\n", s);

        return;

    }
    if (bind(s, (struct sockaddr *) &serverAddr, sockAddrSize) == ERROR)

    {

        printf("Error: www: bind\n");

        return;

    }
    if (listen(s, SERVER_MAX_CONNECTIONS) == ERROR)
    {

        perror("www listen");

        close(s);

        return;

    }
    printf("www server running\n");
    for(;;)
    {
        int size = sizeof(clientAddr);
        int client = accept(s, &clientAddr, &size);
        
        if (client > 0)
        {
            printf("client connected\n");
            //TASK_ID ws_construct=taskSpawn("tGetHTML", 105, VX_FP_TASK, 4096, (FUNCPTR) makeSite_r,0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
           
        /* The client connected from IP address inet_ntoa(clientAddr.sin_addr)

        and port ntohs(clientAddr.sin_port).

		
		
        Start a new task for each request. The task will parse the request
        and sends back the response.
        Don't forget to close newFd at the end */
    
		genarateHTML(client);
            //send(client, new_str2, strlen(new_str2), 0);
            close(client);
			//free(new_str2);			
        }
        storeVal(desired_pose,actual_pose,pwm_value);
        DesPos_max=findMax(DesPos_data);
        DesPos_min=findMin(DesPos_data);
        ActPos_max=findMax(ActPos_data);
        ActPos_min=findMin(ActPos_data);
        if(abs(ActPos_min)>ActPos_max)
        {
        	ActPos_max=abs(ActPos_min);
        }
        if(abs(DesPos_min)>DesPos_max)
        {
        	DesPos_max=abs(DesPos_min);
        }
        if(abs(ActPos_min)<ActPos_max)
        {
        	ActPos_min=-ActPos_max;
        	
        }
        if(abs(DesPos_min)<DesPos_max)
        {
        	DesPos_min=-DesPos_max;
        }
     
        taskDelay(100);
    }
}


void irc_isr_rutine(void)
{
    past_values[0] = current_values[0];
    past_values[1] = current_values[1];
    
    bool irc_a = (MOTOR_SR & BIT(MOTOR_SR_IRC_A_MON)) != 0;
    bool irc_b = (MOTOR_SR & BIT(MOTOR_SR_IRC_B_MON)) != 0;
    
    current_values[0] = irc_a;
    current_values[1] = irc_b;

    if(((current_values[0] == 0)&&(current_values[1] == 0))&&((past_values[0] == 0)&&(past_values[1] == 1)))
    {
        actual_pose += INCREMENT;
    }
    if((current_values[0] == 1)&&(current_values[1] == 0)&&(past_values[0] == 0)&&(past_values[1] == 0))
    {
        actual_pose += INCREMENT;
    }
    if((current_values[0] == 1)&&(current_values[1] == 1)&&(past_values[0] == 1)&&(past_values[1] == 0))
    {
        actual_pose += INCREMENT;
    }
    if((current_values[0] == 0)&&(current_values[1] == 1)&&(past_values[0] == 1)&&(past_values[1] == 1)){
        actual_pose += INCREMENT;
    }
                                
    if((current_values[0] == 0)&&(current_values[1] == 0)&&(past_values[0] == 1)&&(past_values[1] == 0))
    {
        actual_pose -= INCREMENT;
    }
    if((current_values[0] == 0)&&(current_values[1] == 1)&&(past_values[0] == 0)&&(past_values[1] == 0))
    {
        actual_pose -= INCREMENT;
    }
    if((current_values[0] == 1)&&(current_values[1] == 1)&&(past_values[0] == 0)&&(past_values[1] == 1))
    {
        actual_pose -= INCREMENT;
    }
    if((current_values[0] == 1)&&(current_values[1] == 0)&&(past_values[0] == 1)&&(past_values[1] == 1))
    {
        actual_pose -= INCREMENT;
    }

    irq_count++;
    GPIO_INT_STATUS = MOTOR_IRQ_PIN; /* clear the interrupt */
}

void irc_init_l(void)
{
    GPIO_INT_STATUS = MOTOR_IRQ_PIN; /* reset status */
    GPIO_DIRM = 0x0;                 /* set as input */
    GPIO_INT_TYPE = MOTOR_IRQ_PIN;   /* interrupt on edge */
    GPIO_INT_POLARITY = 0x0;         /* falling edge */
    GPIO_INT_ANY = 0x0;              /* ignore rising edge */
    GPIO_INT_ENABLE = MOTOR_IRQ_PIN; /* enable interrupt on MOTOR_IRQ pin */

    intConnect(INUM_TO_IVEC(INT_LVL_GPIO), irc_isr_rutine, 0);
    intEnable(INT_LVL_GPIO);         /* enable all GPIO interrupts */
}

void irc_cleanup_(void)
{
    GPIO_INT_DISABLE = MOTOR_IRQ_PIN;

    intDisable(INT_LVL_GPIO);
    intDisconnect(INUM_TO_IVEC(INT_LVL_GPIO), irc_isr_rutine, 0);
}

void UDP_listener (char * ip_addr,int port)
{
	int sockd;
	struct sockaddr_in my_name, cli_name;
	char buf[MAX_BUF];
	int status;
	int addrlen;
	
	sysClkRateSet(1000);
	
	printf("Started\n");
	/* Create a UDP socket */
	sockd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockd == -1)
	{
		perror("Socket creation error");
		exit(1);
	}
	/* Configure server address */
	my_name.sin_family      = AF_INET;
	my_name.sin_addr.s_addr = INADDR_ANY;
	my_name.sin_port        = htons(port);
	
	
	status  = bind(sockd, (struct sockaddr*)&my_name, sizeof(my_name));
	addrlen = sizeof(cli_name);
	printf("UDP socket created\n");
	
	TASK_ID ws;
	TASK_ID pid;
	irc_init_l();
	init_motor_r();
	
	pid = taskSpawn("tMotorController", 90, VX_FP_TASK, 4096, (FUNCPTR) pwm_control, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	ws  = taskSpawn("tWebServer", 105, 0, 4096, (FUNCPTR) www_rutine, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    printf("Tasks crated\n");
    int i=0;
    for(i=0;i<MAX_DATA;i++)
    {
    	 DesPos_data[i] = 0;
    	 ActPos_data[i] = 0;
    	 PWM_data[i] = 0;
    }
    

    for(;;)
    {
		status = recvfrom(sockd, buf, MAX_BUF, 0,
			  (struct sockaddr*)&cli_name, &addrlen);
		printf("BUF: %s\n", buf);
		desired_pose  = atoi(buf); 
		printf("REQ: %d\n",desired_pose);
    }
	close(sockd);
}
