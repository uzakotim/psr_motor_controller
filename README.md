# PSR_motor_controller

# Semestral Project To-do List:
- [DONE] 1) Configure GitLab for Begim
- [DONE] 2) Write code that receives data from encoder of a motor
- [DONE] 3) Implement sending of the data through UDP socket
- [DONE] 4) Write controller for the motor
- [Almost DONE] 5) Write web-server 
