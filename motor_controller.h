
/*! 
 *  \file      motor_controller.h 
 *  \brief     Motor position receiving program.
 *  \details   This code receives motor position from another board and controls the motor position.
 *  \author    Timur Uzakov
 *  \author    Danylo Begim
 *  \version   1.0
 *  \date      2021-2022     
 *  \warning   Improper use can crash your application
 *  \copyright MIT License.
 */

/* PSR - Czech Technical University
 * Authors: Timur Uzakov, Danylo Begim
 */
#ifndef CONTROLLER_H
#define CONTROLLER_H



#include <intLib.h>
#include <iv.h>
#include <kernelLib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <semLib.h>
#include <sockLib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <taskLib.h>
#include <xlnx_zynq7k.h>

/*!
  \def PID_Controller_Priority
  Setting of the priority of controller task
*/
/*!
  \def UDP_Priority
  Setting of the priority of message sending task
*/
/*!
  \def WWW_priority
  Setting of the priority of web server task
*/

#define PID_Controller_Priority 110
#define UDP_Priority 105
#define WWW_priority 120
/*!
  \def  DELAY_BEETWEEN_UDP_MESSAGES
*/

#define DELAY_BEETWEEN_UDP_MESSAGES 100

#include <taskLib.h>
#include <stdio.h>
#include <kernelLib.h>
#include <semLib.h>
#include <intLib.h>
#include <iv.h>
#include <sysLib.h>
#include <inetLib.h>

#include <xlnx_zynq7k.h>

#include <stdint.h>
#include <stdbool.h>
#include <taskLib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <sockLib.h>
#include <string.h>

/*!
  \def REGISTER(base, offs)
  Sets the register address based on motor \a base and motor \a offs parameter
*/

#define REGISTER(base, offs) (*((volatile UINT32 *)((base) + (offs))))
/*!
  \def BIT(i) 
  Changes the \a i  th bit of a register
*/

#define BIT(i) ((1) << (i))
/*!
  \def MAX_BUF 
  Maximal buffer size
*/
#define MAX_BUF 1024
/*!
  \def CLOCK_RATE 
  Parameter for sysClkRateSet() function
*/
#define CLOCK_RATE 1000

/*!
  \def PWM_PERIOD_VALUE 
  Maxiamal PWM period value
*/
#define PWM_PERIOD_VALUE 5000

/*!
  @param irq_count 
  Counter of messages from IRQ
*/

volatile unsigned irq_count;
/* MOTOR macros 		
 * 												*/
/*!
  \def MOTOR_PIN 
*/

#define MOTOR_PIN BIT(6)

/* See section FPGA registers for more information.						*/

/*!
  \def PMOD1_BASE
  Address of motor connection bus 1
*/
#define PMOD1_BASE 0x43c20000
/*!
  \def PMOD2_BASE
  Address of motor connection bus 2
*/
#define PMOD2_BASE 0x43c30000

/*!
  \def MOTOR_BASE
  Address of motor connection 
*/

#define MOTOR_BASE PMOD1_BASE

/* GPIO register definitions											*/
/* See Zynq-7000 Technical Reference Manual for more information		*/
/*     Section: 14.2.4 Interrupt Function (pg. 391, pg. 1348).			*/

/*!
 * \def MOTOR_IRQ_PIN
 *  Pin on GPIO selected for interrupt										
 *  Note: Each bit in a register refers to one pin. Setting some bit to `1`		
 *  also means which pin is selected.									
*/
#define MOTOR_IRQ_PIN BIT(2)
/*!
 * \def GPIO_DIRM
 * Setting a bit in DIRM to `1` makes the corresponding pin behave as an output,
 * for `0` as input.															
 * Note: So setting this to `MOTOR_IRQ_PIN` means, that this pin is an output	
 *       (which it is not so do not do it!).									
 *       This is similar with other GPIO/INT registers.										
*/ 
#define GPIO_DIRM         REGISTER(ZYNQ7K_GPIO_BASE, 0x00000284)

/*!
 * \def GPIO_INT_ENABLE
 * Writing 1 to a bit enables IRQ from the corresponding pin.	
*/ 
#define GPIO_INT_ENABLE   REGISTER(ZYNQ7K_GPIO_BASE, 0x00000290)
/*!
 * \def GPIO_INT_DISABLE
 * Writing 1 to a bit disables IRQ from the corresponding pin.					
*/
#define GPIO_INT_DISABLE  REGISTER(ZYNQ7K_GPIO_BASE, 0x00000294)
/*!
 * \def GPIO_INT_STATUS 
 * Bits read as `1` mean that the interrupt event has occurred on a corresponding pin.	
 * Writing `1` clears the bits, writing `0` leaves the bits intact.						
*/
#define GPIO_INT_STATUS   REGISTER(ZYNQ7K_GPIO_BASE, 0x00000298)
/*!
 * \def GPIO_INT_TYPE 
 * Setting TYPE to `0` makes interrupt level sensitive, `1` edge sensitive.			
*/
#define GPIO_INT_TYPE     REGISTER(ZYNQ7K_GPIO_BASE, 0x0000029c)
/*!
 * \def GPIO_INT_POLARITY 
 * Setting POLARITY to `0` makes interrupt active-low (falling edge),					
 *                     `1` active-high (raising edge).									
*/
#define GPIO_INT_POLARITY REGISTER(ZYNQ7K_GPIO_BASE, 0x000002a0)
/*!
 * \def GPIO_INT_ANY 
 * Setting ANY to `1` while TYPE is `1` makes interrupts act on both edges.
*/
#define GPIO_INT_ANY      REGISTER(ZYNQ7K_GPIO_BASE, 0x000002a4)

/* FPGA register definition																*/
/*!
 * \def MOTOR_SR REGISTER 
 * FPGA register definition																
*/
#define MOTOR_SR REGISTER(MOTOR_BASE, 0x4)
/*!
 * \def MOTOR_SR_IRC_A_MON
 * FPGA register definition																
*/
#define MOTOR_SR_IRC_A_MON 8
/*!
 * \def MOTOR_SR_IRC_B_MON
 * FPGA register definition																
*/
#define MOTOR_SR_IRC_B_MON 9

/*!
 * \def INCREMENT
 * Encoder value increment, always equal to 1																
*/
#define INCREMENT 1

/*!
 * \def MAX_DATA
 * Array size for data for web server																
*/
#define MAX_DATA 400

/*!
 * \def PWM_MAX
 * PWM threhold value																
*/
#define PWM_MAX 300

int PIDReqValue;
int IRQCounter;
float PID_OUT;
char * IP_Target;
char * IP_BOARD;

int PWM_data[MAX_DATA ];
int DesPos_data[MAX_DATA ];
int ActPos_data[MAX_DATA ];
unsigned int Time_data[MAX_DATA];

/* MOTOR macros 														*/

/* See section FPGA registers for more information.						*/

/*!
 * \def PWM_ENABLE
 * PWM enabling register																
*/
#define PWM_ENABLE        REGISTER(PMOD1_BASE, 0x0000)
/*!
 * \def PWM_PERIOD
 * PWM max period value setter register															
*/
#define PWM_PERIOD        REGISTER(PMOD1_BASE, 0x0008) 
/*!
 * \def DUTY_PWM
 * PWM duty cycle value register																
*/
#define DUTY_PWM          REGISTER(PMOD1_BASE, 0x000C) 
/*!
 * \def IRC_IRQ_MON
 * IRC register																
*/

#define IRC_IRQ_MON       REGISTER(ZYNQ7K_GPIO_BASE, 0x0004) 
/*!
 * \def IRC_B_MON
 * IRC register B channgel																
*/

#define IRC_B_MON         REGISTER(ZYNQ7K_GPIO_BASE, 0x0004) 
/*!
 * \def IRC_A_MON
 * IRC register A channgel																
*/

#define IRC_A_MON         REGISTER(ZYNQ7K_GPIO_BASE, 0x0004) 
/*!
 * \def PWM_PERIOD_VALUE
 * MAX PWM_PERIOD value																
*/
#define PWM_PERIOD_VALUE  5000

/* WEB SERVER macros                                                     */
/*!
 * \def SERVER_PORT
 * server port default is 80																
*/
#define SERVER_PORT     80 /* Port 80 is reserved for HTTP protocol */
/*!
 * \def SERVER_MAX_CONNECTIONS
 * server limit of connections, set to 20							
*/
#define SERVER_MAX_CONNECTIONS  20











/*!
    \fn  void init_motor_r(void)
    \brief This function receives position from another motor and stores it in variable desired_pose
*/
void init_motor_r(void);

/*! \fn void pose_getter_r(int port)
 *  \brief This function receives position from another motor and stores it in variable desired_pose
    \param port port number
*/
void pose_getter_r(int port);

/*! \fn void pwm_control(void)
 *  \brief This function calculates value of PWM by using PI controller
 * 
*/
void pwm_control(void);


/*! \fn void genarateHTML(int fd)
 *  \brief  This function generates html code and return it as char array
    \param fd file descriptor
*/
void genarateHTML(int fd);


/*! \fn  void www_rutine(int argc, char *argv[])
 *  \brief This function is web server routine task 
    \param argc number of arguments
    \param argv array of input
*/
void www_rutine(int argc, char *argv[]);


/*! \fn void irc_isr_rutine(void)
 *  \briefThis function records values of position from encoder and stores them in actual_pose variable 
*/
void irc_isr_rutine(void);



/*! \fn void irc_init_l(void)
 *  \brief This function connects the IRC sensor
*/
void irc_init_l(void);


/*! \fn void irc_cleanup_(void)
 *  \brief This function disconnects the IRC sensor
 * 
*/
void irc_cleanup_(void);



/*!  \fn void UDP_listener(char * ip_addr,int port)
 *   \brief This program listens at a UDP port specified as command line parameter and once it receives a packet it controls the motor
 *   \param ip_addr ip address of board to connect to
 *   \param port port number
 */
void UDP_listener(char * ip_addr,int port);




/*!  \fn void rotateArr(int arr[], int n,int newval)
 *   \brief This function shifts the input array by one position and adds new value to the last position
 *   \param arr input array
 *   \param n size of array
 *   \param newval new value that is written at the last position
 */
void rotateArr(int arr[], int n,int newval);


/*!  \fn void storeVal(int desired,int actual,int pwm)
 *   \brief This function stores the values from measurements into data arrays for web server
 *   \param desired position value received from another board
 *   \param actual  actual position of the motor from encoder
 *   \param pwm     pwm from the controller
 */
void storeVal(int desired,int actual,int pwm);

/*!  \fn void findMin(int arr[])
 *   \brief This function find the maximum value in the given array
 *   \param arr[] array where maximum number will be finded
 */
int findMax(int arr[]);

/*!  \fn void findMin(int arr[])
 *   \brief This function find the minmum value in the given array
 *   \param arr[] array where minmum number will be finded
 */
int findMin(int arr[]);


#endif
